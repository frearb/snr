from SNR import SNR
import math
import numpy as np 
import pandas as pd
from sklearn import linear_model
from gplearn.genetic import SymbolicRegressor
from gplearn.fitness import make_fitness
from sklearn.metrics import r2_score

data = pd.read_csv("data/Hf_2.csv")
x = data.loc[:,["rA","rB","χA","χA","t","u"]]
y = data["rA"])

x["rO"] = 1.35
# x["rO"] += np.random.random(size=len(y))/10000
z = (x['rA']+x['rO'])/(np.sqrt(2)*(x['rB']+x['rO']))
print(r2_score(z,y))
print(np.corrcoef(z,y))
x = x.values.tolist()

width=[27,2781,5000]
model = SNR(width=[27,2781,5000],same_dimension=[[0,1,6],[2,3]],generations=5,alpha=100/sum(width),min_weight=0.001,
            verbose=1,stopping_criteria=0.005,function_set=['mul','div'],reduction='coff',corrcoef_limit=0.9)
model.train(x,y)
