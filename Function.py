import numpy as np

class make_function(object):

    def __init__(self, function, name, limits, arity):
        self.function = function
        self.name = name
        self.arity = arity
        self.limits = limits

    def validate(self,a,b=None):
        if self.arity == 1 and not [False for temp in a.limits if temp not in self.limits[0]]:
            if self.name=='sqrt':
                return [0,1]
            elif self.name == 'log':
                return [-1,0,1]
            elif self.name == 'abs':
                return [0,1] 
            elif self.name == 'sin':
                return [0,1,-1]
            elif self.name == 'exp':
                return [1]       
        elif self.arity == 2 and not [False for temp in a.limits if temp not in self.limits[0]] and not [False for temp in b.limits if temp not in self.limits[1]]:
            if self.name=='add':
                temp = []
                if 1 in a.limits or 1 in b.limits:
                    temp.append(1)
                if -1 in a.limits or -1 in b.limits:
                    temp.append(-1)
                if 0 in a.limits and 0 in b.limits or 1 in a.limits and -1 in b.limits or 1 in b.limits and -1 in a.limits:
                    temp.append(0)
                return temp
            elif self.name == 'sub':
                temp = []
                if 1 in a.limits or -1 in b.limits:
                    temp.append(1)
                if -1 in a.limits or 1 in b.limits:
                    temp.append(-1)
                if 0 in a.limits and 0 in b.limits or 1 in a.limits and 1 in b.limits or -1 in b.limits and -1 in a.limits:
                    temp.append(0)
                return temp
            elif self.name == 'mul':
                temp = []
                if 1 in a.limits and 1 in b.limits or -1 in a.limits and -1 in b.limits:
                    temp.append(1)
                if 1 in a.limits and -1 in b.limits or -1 in a.limits and 1 in b.limits:
                    temp.append(-1)
                if 0 in a.limits or 0 in b.limits:
                    temp.append(0)
                return temp
            elif self.name == 'div':
                temp = []
                if 1 in a.limits and 1 in b.limits or -1 in a.limits and -1 in b.limits:
                    temp.append(1)
                if 1 in a.limits and -1 in b.limits or -1 in a.limits and 1 in b.limits:
                    temp.append(-1)
                if 0 in a.limits:
                    temp.append(0)
                return temp
            elif self.name == 'max':
                temp = []
                if 1 in a.limits or 1 in b.limits:
                    temp.append(1)
                if -1 in a.limits and -1 in b.limits:
                    temp.append(-1)
                if 1 not in a.limits and 1 not in b.limits and (0 in a.limits or 0 in b.limits):
                    temp.append(0)
                return temp
            elif self.name == 'min':
                temp = []
                if -1 in a.limits or -1 in b.limits:
                    temp.append(-1)
                if 1 in a.limits and 1 in b.limits:
                    temp.append(1)
                if -1 not in a.limits and -1 not in b.limits and (0 in a.limits or 0 in b.limits):
                    temp.append(0)
                return temp
            elif self.name == 'pow':
                return [1]
        else:
            print('error')
            return [2]

    def __call__(self, *args):
        return self.function(*args)

add2 = make_function(function=np.add, name='add', limits=[[-1,0,1],[-1,0,1]], arity=2)
sub2 = make_function(function=np.subtract, name='sub', limits=[[-1,0,1],[-1,0,1]], arity=2)
mul2 = make_function(function=np.multiply, name='mul', limits=[[-1,0,1],[-1,0,1]], arity=2)
div2 = make_function(function=np.divide, name='div', limits=[[-1,0,1],[-1,1]], arity=2)
sqrt1 = make_function(function=np.sqrt, name='sqrt', limits=[[0,1]], arity=1)
log1 = make_function(function=np.log, name='log', limits=[[1]], arity=1)
abs1 = make_function(function=np.abs, name='abs', limits=[[-1,0,1]], arity=1)
max2 = make_function(function=np.maximum, name='max', limits=[[-1,0,1],[-1,0,1]], arity=2)
min2 = make_function(function=np.minimum, name='min', limits=[[-1,0,1],[-1,0,1]], arity=2)
sin1 = make_function(function=np.sin, name='sin', limits=[[-1,0,1]], arity=1)
exp1 = make_function(function=np.exp, name='exp', limits=[[-1,0,1]], arity=1)
pow1 = make_function(function=np.power,name='pow', limits=[[1],[-1,0,1]],arity=2)
# demo
function_map = {
    'add': add2,
    'sub': sub2,
    'mul': mul2,
    'div': div2,
    'sqrt': sqrt1,
    'log': log1,
    'abs': abs1,
    'max': max2,
    'min': min2,
    'sin': sin1,
    'exp': exp1,
    'pow': pow1,
}
