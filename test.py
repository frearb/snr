from SNR import SNR
import math
import numpy as np 
from sklearn import linear_model
from gplearn.genetic import SymbolicRegressor
from gplearn.fitness import make_fitness
from sklearn.metrics import r2_score
import time
from sympy import *

def express(ex):
        S = symbols("x0,x1,x2,x3")
        locals = {
            "add": Add,
            "mul": Mul,
            "sub": Lambda((S[0], S[1]), S[0] - S[1]),
            "div": Lambda((S[0], S[1]), S[0]/S[1]),
            "neg": Lambda(S[0],-S[0]),
            "X0": S[0],"X1": S[1],"X2": S[2],"X3":S[3]
            }
        return sympify(ex,locals=locals)

np.random.seed(7)
x0 = np.random.random(size=1000)
x1 = np.random.random(size=1000)
x2 = np.random.normal(0,2,size=1000)
x = np.transpose([x0,x1,x2])
y = (x2/np.sqrt(x1))*np.log(x1)/x0

print(express('(x2/sqrt(x0+x1))*log(x1)/x0'))
print(x[:5])
print(y[:5])

if __name__ == '__main__':
    ## 使用SNR模型
    model = SNR(width=[27,2187,5000],generations=20,alpha=0.3,min_weight=0.01,
                verbose=1,stopping_criteria=0.02,function_set=['mul','div','sqrt','log','add','sub'],limits = [[1],[1],[0,1,-1]])
    time_start=time.time()
    model.train(x,y)
    time_end=time.time()
    print('time cost',time_end-time_start,'s')
    # result = model.predict(x)
    # print(result)

    ## 使用lasso
    clf = linear_model.Lasso(alpha=0.001,normalize=True)

    clf.fit(x,y)
    print(clf.coef_,clf.score(x,y))

    ## 使用gplearn
    def r2(y_true, y_pred, sample_weight=None):
        return r2_score(y_true, y_pred, sample_weight, multioutput='uniform_average')
    metric = make_fitness(function=r2,greater_is_better=True)
    est_gp = SymbolicRegressor(population_size=5000,function_set=('add', 'sub', 'mul', 'div','log','sqrt'),
                               generations=20, stopping_criteria=0.05,
                               p_crossover=0.7, p_subtree_mutation=0.1,
                               p_hoist_mutation=0.05, p_point_mutation=0.1,
                               max_samples=0.9, verbose=1,
                               parsimony_coefficient=0.01, random_state=0,
                               )
    time_start=time.time()
    est_gp.fit(x, y)
    time_end=time.time()
    print('time cost',time_end-time_start,'s')
    print(express(est_gp._program))
    print(est_gp.score(x,y))
