from Function import function_map
from random import randint
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.feature_selection import VarianceThreshold
import numpy as np
# from multiprocessing import Pool
from functools import partial
from joblib import Parallel, delayed
from sympy import *

def logs(typ,log):
    print(log)

def _parallel_update(symbolic_network,width,data):
    new_x = []
    for node in symbolic_network[0]:
        new_x.append(node.update(data))
    for i in range(1,len(width)):
        for j in range(width[i]):
            node = symbolic_network[i][j]
            temp = node.update()
            assert temp > -np.inf and temp <np.inf , str(node)+' '+node.info()
            new_x.append(temp)
    return new_x

class Node():

    def __init__(self,is_leaf=False,child_left=None,function=None,child_right=None,index=None,limits=None):
        self.is_leaf = is_leaf
        if is_leaf:
            self.index = index
            self.lenth = 1
            if limits:
                self.limits = limits
            else:
                self.limits = [1]
        else:   
            self.function = function
            self.child_left = child_left
            self.child_right = child_right
            self.limits = self.function.validate(child_left,child_right)
            if self.child_right:
                self.lenth = self.child_left.lenth + self.child_right.lenth+1
            else:
                self.lenth = self.child_left.lenth + 1

    def rebuild(self,function,child_left,child_right=None):
        self.function = function
        self.child_left = child_left
        self.child_right = child_right
        self.limits = self.function.validate(child_left,child_right)
        if self.child_right:
            self.lenth = self.child_left.lenth + self.child_right.lenth+1
        else:
            self.lenth = self.child_left.lenth + 1

    def update(self,data=None):
        if self.is_leaf:
            self.value = data[self.index]
        elif self.function.arity==1:
            self.value = self.function(self.child_left.value)
        else:
            self.value = self.function(self.child_left.value,self.child_right.value)
        return self.value

    def __str__(self):
        if self.is_leaf:
            return 'X'+str(self.index)
        elif self.function.arity == 2:
            return self.function.name+'('+str(self.child_left)+','+str(self.child_right)+')'
        else:
            return self.function.name+'('+str(self.child_left)+')'
    
    def info(self):
        if self.is_leaf:
            return 'X'+str(self.index)+' '+str(self.limits)
        elif self.function.arity == 2:
            return '('+self.child_left.info()+' '+self.function.name+' '+self.child_right.info()+' '+str(self.limits)+')'
        else:
            return '('+self.function.name+' '+self.child_left.info()+' '+str(self.limits)+')'
            

class SNR:
    def __init__(self,
                 function_set=['mul','div','log','sqrt'],
                 width=[50,50,50],
                 stopping_criteria=0.1,
                 metric='mean absolute error',
                 n_jobs=1,
                 verbose=0,
                 generations=100,
                 same_dimension=[],
                 alpha=1,
                 min_weight=0,
                 corrcoef_limit=0.95,
                 reduction = 'hash',
                 limits = None,
                 test = 0.5):
        self.function_set = function_set
        self.width = [0]+width
        self.stopping_criteria = stopping_criteria
        self.metric = metric
        self.n_jobs = n_jobs
        self.verbose = verbose
        self.generations = generations
        self.same_dimension = same_dimension
        self.clf = linear_model.Lasso(alpha=alpha,normalize=True,selection='random')
        self.min_weight = min_weight
        self.corrcoef_limit = corrcoef_limit
        self.reduction = reduction
        self.limits = limits
        self.test = test

    def init_network(self):
        self.symbolic_network = []
        temp = []
        for i in range(self.n):
            temp.append(Node(is_leaf=True,index=i,limits=self.limits[i]))
        self.width[0] = self.n
        self.symbolic_network.append(temp)   ## 构建网络第零层，base_layer
        # temp = []
        # for i in self.same_dimension:
        #     for j in range(len(i)):
        #         for k in range(j+1,len(i)):
        #             temp.append(Node(function=function_map["add"],child_left=self.symbolic_network[0][i[j]],child_right=self.symbolic_network[0][i[k]]))
        #             temp.append(Node(function=function_map["sub"],child_left=self.symbolic_network[0][i[j]],child_right=self.symbolic_network[0][i[k]]))
        # self.width[1] = len(temp)
        # self.symbolic_network.append(temp)  ## 构建网络第一层，add&sub_layer, 可以为空
        for i in range(1,len(self.width)):
            temp = []
            for j in range(self.width[i]):
                function = function_map[self.function_set[randint(0,len(self.function_set)-1)]]
                n = sum(self.width[:i])
                node =self.get_node(randint(0,n-1),i,n,function.limits[0])
                if function.arity == 2:
                    node2 =self.get_node(randint(0,n-1),i,n,function.limits[1])
                    assert not [False for temp in node2.limits if temp not in function.limits[1]] , 'init get node wrong'
                    temp.append(Node(function=function,child_left=node,child_right=node2))
                else:
                    temp.append(Node(function=function,child_left=node))
            self.symbolic_network.append(temp) ## 构建网络其他层
        # self.print()
    
    def update_network(self):
        count = 0
        ## 初始化权重
        for i in range(len(self.width)):
            for j in range(self.width[i]):
                self.symbolic_network[i][j].weight = self.weights[count]
                count+=1
        ## 反向传播权重
        for i in range(2,len(self.width))[::-1]:
            for j in range(self.width[i]):
                node = self.symbolic_network[i][j]
                if node.weight != 0:
                    if not node.child_left.is_leaf:
                        node.child_left.weight += node.weight
                    if node.function.arity == 2 and not node.child_right.is_leaf:
                        node.child_right.weight += node.weight
        ## 正向重建网络
        for i in range(1,len(self.width)):
            for j in range(self.width[i]):
                node = self.symbolic_network[i][j]
                if node.weight <= self.min_weight:
                    function = function_map[self.function_set[randint(0,len(self.function_set)-1)]]
                    n = sum(self.width[:i])
                    node1 =self.get_node(randint(0,n-1),i,n,function.limits[0])
                    if function.arity == 2:
                        node2 =self.get_node(randint(0,n-1),i,n,function.limits[1])
                        node.rebuild(function,node1,node2)
                        assert not [False for temp in node2.limits if temp not in function.limits[1]] , 'rebuilding get node wrong'
                    else:
                        node.rebuild(function,node1)

    def get_node(self,index,layer,n,limits):
        index_copy = index
        if index < 0:
            index += n
        for i,j in enumerate(self.width[:layer]):
            if index >= j:
                index -= j
            elif [False for temp in self.symbolic_network[i][index].limits if temp not in limits]:
                return self.get_node(index_copy-1,layer,n,limits=limits)
            else:
                return self.symbolic_network[i][index]

    def update(self,data):
        new_x = []
        # pool = Pool(3)
        for node in self.symbolic_network[0]:
            new_x.append(node.update(data))
        for i in range(1,len(self.width)):
            for j in range(self.width[i]):
                node = self.symbolic_network[i][j]
                temp = node.update()
                assert np.sum(np.isnan(temp))==0 and np.sum(np.isinf(temp))==0 ,str(node)+' '+node.info()
                new_x.append(temp)
            # result=pool.map(partial(self.update_sub,i=i),range(self.width[i]))
            # new_x += result
        new_x = np.around(new_x,decimals=8).tolist()
        return new_x
    def hash_reduction(self,xx):
        assert np.sum(np.isnan(xx)) == 0 and np.sum(np.isinf(xx)) == 0 , 'nan: '+str(np.sum(np.isnan(xx)))+' inf: '+str(np.sum(np.isinf(xx)))
        right = []
        xx_hash = {}
        for i in range(len(xx)):
            temp = hash(tuple(xx[i]))
            if xx_hash.get(temp) is None:
                xx_hash[temp] = i
                right.append(i)
        xx_new = np.array(xx)[right].T.tolist()
        vt = VarianceThreshold(threshold=(.8 * (1 - .8)))
        xx_new = vt.fit_transform(xx_new)
        right = np.array(right)[vt.get_support()].tolist()
        return right, xx_new

    def feature_reduction(self,xx):
        xx_temp = np.array(xx).T.tolist()
        assert len(xx_temp) > 0
        n = len(xx_temp[0])
        with np.errstate(invalid='ignore'):
            corr = np.corrcoef(xx_temp,rowvar=0)
        right = []
        for i in range(n):
            relate = False
            for j in right:
                if np.isnan(corr[i][i]) or np.abs(corr[i][j]) >self.corrcoef_limit:
                    relate = True
                    break
            if not relate:
                right.append(i)
        xx_new = np.array(xx_temp).T[right].T
        return right,xx_new

    def train(self,x,y):
        self.n = len(x[0])
        if not self.limits:
            self.limits = [1] * self.n
        self.init_network()
        x_transpose = np.transpose(x).astype(float).tolist()
        for i in range(self.generations):
            xx = self.update(x_transpose)
            if self.reduction == 'hash':
                xx_index, xx_new = self.hash_reduction(xx)
            elif self.reduction == 'coff':
                xx_index,xx_new = self.feature_reduction(xx)
            else:
                print('argument error')
                return
            x_train,x_test,y_train,y_test = train_test_split(xx_new,y,test_size=self.test)
            self.clf.fit(x_train, y_train)
            clf_weights = self.clf.coef_
            self.weights = [0 for j in range(len(xx))]
            for j in range(len(clf_weights)):
                self.weights[xx_index[j]] = clf_weights[j]
            self.weights = np.abs(self.weights)
            score = self.clf.score(x_test,y_test)
            score_train = self.clf.score(x_train,y_train)
            if self.verbose:
                print("<generation:%d>"%i)
                print(score,score_train,len(xx),len(xx_new[0]))
                # print(np.corrcoef(xx,rowvar=0))
                result = []
                for j in range(len(self.weights)):
                    if self.weights[j] > self.min_weight:
                        ex = self.index_node(j)
                        result.append((round(self.weights[j],6),self.express(ex)))
                result = sorted(result,key=lambda s: s[0],reverse = True)
                for j in result:
                    print(j)
                # self.print()
                # print(self.weights)
                print("")
            if score >= (1-self.stopping_criteria):
                return
            else:
                self.update_network()

    def predict(self,x):
        xx = []
        for j in x:
            xx.append(self.update(j))
        return self.clf.predict(xx)
    
    def print(self):
        for i,j in enumerate(self.width):
            for k in range(j):
                print(self.symbolic_network[i][k])

    def index_node(self,index):
        for i,j in enumerate(self.width):
            if index >= j:
                index -= j
            else:
                return self.symbolic_network[i][index]
    def express(self,ex):
        S = symbols("x0,x1,x2,x3")
        locals = {
            "add": Add,
            "mul": Mul,
            "sub": Lambda((S[0], S[1]), S[0] - S[1]),
            "div": Lambda((S[0], S[1]), S[0]/S[1]),
            "neg": Lambda(S[0],-S[0]),
            "X0": S[0],"X1": S[1],"X2": S[2],"X3":S[3]
            }
        return sympify(ex,locals=locals)
        
